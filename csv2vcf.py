#!/usr/bin/env python3
import argparse
import csv
from pathlib import Path


def get_headings(filename):
    """Get headings from heading file"""
    with open(filename, 'r', encoding="utf-8") as f:
        # csv_reader = csv.reader(f)
        data = f.readlines()
        return data[1].split(',')

def get_contacts_csv_file(filename):
    """Read CSV file"""
    contacts = []
    fieldnames = get_headings('entetes.csv')

    with open(filename, 'r', encoding="utf-8") as csvfile:
        dict_reader = csv.DictReader(csvfile, fieldnames=fieldnames)
        for row in dict_reader:
            contacts.append(row)
    return contacts

def write_contacts_vcard_file(filename, contacts):
    """Write contacts in VCARD file"""
    with open(filename, 'w', encoding="utf-8") as f:
        for contact in contacts[1:]:
            fn = contact['contact'] if contact['contact'] != '' else contact['name']
            f.write("BEGIN:VCARD\n")
            f.write("VERSION:4.0\n")
            f.write(f"FN:{fn}\n")
            if contact['email']:
                f.write(f"EMAIL;TYPE=WORK:{contact['email']}\n")
            if contact['tel']:
                f.write(f"TEL;TYPE=CELL:{contact['tel']}\n")
            f.write(f"ORG:{contact['name']}\n")
            f.write(f"URL;VALUE=URI:{contact['website']}\n")
            f.write(f"CATEGORIES:Numérique libre\n")
            f.write("END:VCARD\n")


if __name__ == "__main__":
    """Run as script"""
    parser = argparse.ArgumentParser()
    parser.add_argument("filename", help="CSV filename", type=str)
    parser.add_argument("-o", "--output", help="Output VCard filename", type=str)
    args = parser.parse_args()

    contacts = get_contacts_csv_file(args.filename)
    write_contacts_vcard_file(args.output, contacts)
